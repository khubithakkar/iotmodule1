## Industrial Revolution
![industrial revolution](pictures/industRev.png)
1. **Industry 1.0** is period where mechanization was introduced using steam and water power.  
2. **Industry 2.0** is the period where mass production began with assembly lines and introduction to electrical systems.  
3. **Industry 3.0** is the digitization of manufacturing process and introduction to web-based services, autonomous robots, nanotechnology,etc.  
4. **Industry 4.0** is the current trend of automation, data-exchange and cyberphysical systems in manufachturing technologies.

## Industry 3.0
![Workflow of Industry3.0](pictures/indust3.png)

### Architecture of Industry 3.0
![Parts of Industry 3.0](pictures/arch3.png)

* Sensors, Actuators and RTU(Remote Terminal Unit) are Field Devices and they send data to the Control Devices to ensure the working of industry.
* Communication between Field Devices and Control Devices is done by Fieldbus
* Controllers, CNC(Computer Numerical Control), PLC(Programmable Logic Control), DCS(Digital Control System) are the Control Devices.
* Historian is responsible for storing and logging all of the data that the SCADA system aggregates.
* ERP(Enterprise Resource Planning) and MES(Manufacturing Execution Systems) are manufacturing environments. 
1. ERP functions as a means for sharing information within an organization, it gives management to dive deep and tie data in meaningful ways.
2. MES provides precise control of manufacturing process, it synchronizes the numerous facets of fabrication to orchastrate the best possible solution for obtaining a less wasteful and more profitable process.

### Communication Protocols of Industry 3.0
![Types of Protocols](pictures/proto3.png)

## Industry 4.0
![Components of Industry 4.0](pictures/first.jpg)

### Structure of Industry 4.0
![structure](pictures/ind4.png)

### Difference between Industry 3.0 and Industry 4.0
![difference](pictures/two.png)

### Advantages on Industry 4.0
![advantages](pictures/adv4.png)

1. **Inventory Management**: Monitoring events across supply chains. Provides cross-channel visibility into inventories and managers are provided with realistic estimates of the available material, work in progress and the estimated arrival time of new materials. Ultimately this optimizes supply and reduces shared costs in the value chain.

2. **Quality Control**: IoT sensors collect aggregate product data and other third-party syndicated data from various stages of a product cycle. This data relates to the complete  composition of raw materials used, temperature and working environment, wastes, the impact of transportation and more on the final products.

3. **Enhanced Safety**: Key performance indicators of health and safety, such as number of injuries, short- and long-term absences, illness rates, near-misses, can thus be monitored constantly to ensure better workplace conditions.

4. **Smart Metering**: These Smart meters can monitor the consumption of resources like electricity, fuels, water etc.Through effective management, operational expenditure can be reduced significantly.

5. **Predictive Maintenance**:  Predictive maintenance (PdM) techniques are designed to help determine the condition of equipments in order to predict when maintenance should be performed or possible breakdowns. This promises cost savings over routine or time-based preventive maintenance, as the tasks are performed only when warranted.

6. **Smart Packaging**: Offers insights that can be used to re-engineer products and packaging for better performance in both customer experience and at times, even the cost of packaging.

7. **Digital Industries**: IoT enabled machinery can transmit operational information to the partners like OEMs (original equipment manufacturers) and to field engineers. This will enable operation managers and factory heads to remotely manage the factory units and take advantage of process automation and optimization. This makes streamlining the day-to-day work effortless.

### Communication Protocol for Industry 4.0
![protocols](pictures/proto4.png)

### Problems with Industry 4.0
1. **Cost** We will need additional hardware for converting and industry 3.0 to industry 4.0 which are expensive. Also some Devices that are not compatible for this might need to be changed for complete automation.
2. **Downtime** Changing hardware means shutting down the factory during installations 
3. **Reliablity** One does not want to invest in devices that are unproven or unreliable.
#### Solution 
Get data from industry 3.0 devices/meters/sensors without changes to the original device and send the data to the Cloud using Industry 4.0 devices.

## How to converte Industry 3.0 communications protocols to Industry 4.0
**Problems** 
Expensive Hardware  
Lack of Documentation  
Properitary PLC protocols  
**Solution**
There are companies that provides devices and support in that.
![](pictures/bs.png)

**Roadmap**
*Step1:* Identifiy most popular Industry 3.0 devices
*Step2:* Study Protocols that these devices communicate
*Step3:* Get data from the industry 3.0 devices
*Step4:* Send the data to loud for industry 4.0

## After storing the Data to the Internet..
...you can use the following services  
1. IoT TSDB tools  
Store your data in time series data bases  

![](pictures/tsdb.png)

2. IoT Dashboards  
Veiw all your data into dashboards  

![](pictures/dashboards.png)

3. IoT Platforms  
Analyse your data on these platforms  

![](pictures/platforms.png)

4. Get Alerts  
Get Alerts based on your data using these platforms  

![](pictures/alerts.png)
